from ast import Del
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy
from django.shortcuts import redirect

from todos.models import TodoItem, TodoList

# Create your views here.

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoDetailView(DeleteView):
    model = TodoList
    template_name = "todos/detail.html"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context[]


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list")

    def form_valid(self, form):
        return super().form_valid(form)

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list")

class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todo_list")

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    def get_success_url(self):
        return redirect("todo_list")