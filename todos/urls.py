from django.urls import path
from django.conf.urls import include

from todos.views import(
    TodoListView,
    TodoCreateView,
    TodoUpdateView,
    TodoDeleteView,
    TodoDetailView,
    TodoItemCreateView,
    TodoItemUpdateView,
)

urlpatterns = [
    path("",TodoListView.as_view(), name="todo_list"),
    path("create/", TodoCreateView.as_view(), name = "todo_new"),
    path("<int:pk>", TodoDetailView.as_view(), name = "todo_detail"),
    path("<int:pk>/edit",TodoUpdateView.as_view(), name = "todo_edit"),
    path("<int:pk>/delete",TodoDeleteView.as_view(), name = "todo_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_new"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name = "todo_item_edit"),
]
